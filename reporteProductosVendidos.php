<?php

include ("Main/Comunes.php");

session_start();

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];

    $db = new Conexion();
    $db->set_charset("UTF8");

    $reporte = $db->query("select nombre_tienda as Tienda, nombre_Caramelo as Caramelo, count(PG.fk_caramelo) as Ventas
    from caramelo, pedido_pago PG, tienda, pedido P
    where cod_Caramelo = PG.fk_Caramelo and PG.fk_Pedido =  P.cod_Pedido and P.fk_Tienda = cod_Tienda
    group by Tienda, Caramelo  
    order by Ventas desc;");
    $reporte = $db->recorrer($reporte);
    //var_dump($reporte);

    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Reporte',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        'tienda' => $_SESSION['tienda'],
        'reporte' => $reporte
    ));
    $template->display("Public/reporteProductosVendidos.tpl");
}
?>