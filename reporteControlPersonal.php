<?php

include ("Main/Comunes.php");

session_start();

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];

    $db = new Conexion();
    $db->set_charset("UTF8");

    $reporte = $db->query("SELECT Nombre_personal, CI_personal,(select sum(timediff(FechaSalida_CL, FechaInicial_CL)) from control_laboral CL, horario_personal HP
    where CL.fk_personal = CI_personal and HP.fk_personal = CI_personal) as 'Horas trabajadas', sec_to_time(avg(time_to_sec(FechaInicial_CL))) as 'Promedio de entrada',sec_to_time(AVG(TIME_TO_SEC(FechaSalida_CL))) as 'Promedio de salida',
    (select count(FechaInicial_CL) from control_laboral where time(FechaInicial_CL)!= '08:00:00' and fk_personal = CI_personal ) as 'Total de Retardos'
    from control_laboral CL, personal, horario_personal HP
    where CI_personal = CL.fk_personal and CI_personal = HP.fk_Personal
    group by Nombre_personal, CI_personal;");
    $reporte = $db->recorrer($reporte);
    //var_dump($reporte);

    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Reporte',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        'tienda' => $_SESSION['tienda'],
        'reporte' => $reporte
    ));
    $template->display("Public/reporteControlPersonal.tpl");
}
?>