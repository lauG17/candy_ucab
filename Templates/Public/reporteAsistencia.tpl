{include file="../Overall/Header.tpl"}

<link rel="stylesheet" href="assets/css/pedidos.css?v={0|rand:100}" media="screen">

<body>
<div id="wrapper">
    {include file='../Overall/Navbar_admin.tpl'}
    <div id="page-wrapper">
        <div class="col-md-12 contenedorPrincipal">
            <div class="col-md-12 columna">
                <br>
                <br>
                <br>

                <div class="row">
                    <div class="col-md-7">
                        <input type="text" class="form-control" placeholder="Buscar por número">
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-info" type="submit">Buscar</button>
                    </div>
                </div>
                <br>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <label>Diario dulce:</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered margenIz ">
                        <thead>
                        <tr>
                            <th width="260" class="text-center info">Nombre</th> <!-- Table Data Head -->
                            <th width="20" class="text-center info">Departamento</th> <!-- Table Data Head -->
                            <th width="20" class="text-center info">Tienda</th>
                            <th width="20" class="text-center info">Entrada</th>
                            <th width="20" class="text-center info">Salida</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach key=key item=item from=$reporte}
                        <tr> <!-- Table Row -->
                            <td width="270">{$item['nombre']} {$item['primerApellido']}</td> <!-- Table Data -->
                            <td width="5" class="text-center">{$item['departamento']}</td> <!-- Table Data -->
                            <td width="5" class="text-center">{$item['tienda']}</td>
                            <td width="5" class="text-center">{$item['entrada']}</td>
                            <td width="5" class="text-center">{$item['salida']}</td>
                        </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-md-offset-10">
                    <br>
                    <button class="btn btn-danger" type="submit">Cancelar Pedido</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

{include file="../Overall/Footer.tpl"}