{include file="../Overall/Header.tpl"}

<link rel="stylesheet" href="assets/css/novedades.css?v={0|rand:100}" media="screen">

<body>

{php}
error_reporting(E_ALL);
ini_set('display_errors', 1);
    echo $tienda = $_smarty_tpl->tpl_vars['tienda']->value;
    $db = new Conexion();
    $query = $db->query("SELECT ctc.cod_CTC idCarameloTC, c.nombre_Caramelo nombreCaramelo, tc.descripcion_TC tipo FROM caramelo c, caramelo_tc ctc, tipo_caramelo tc where ctc.fk_caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC");
    $descuentos = $db->query("SELECT ctc.cod_CTC idCarameloTC, c.nombre_Caramelo nombreCaramelo, tc.descripcion_TC tipo, d.descripcion_descuento porcentaje, date(d.finicio_descuento) fechaIni, date(d.fFinal_descuento) fechaFin FROM caramelo c, caramelo_tc ctc, tipo_caramelo tc, descuento d, cs_descuento csd where ctc.fk_caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC and csd.fk_descuento = d.cod_descuento and csd.fk_Caramelo = ctc.cod_CTC");

{/php}


<div id="wrapper">
    {include file='../Overall/Navbar_admin.tpl'}
    <div id="page-wrapper">
        <div class="col-md-12 contenedorPrincipal">
            <div class="col-md-12 columna">
                <form method="POST" name="addCarameloNovedades" id="addCarameloNovedades" action="novedades.php" validate>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Seleccione un Caramelo:</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <select class="form-control" name="caramelo-tipo" required>
                                <option value="" disabled selected>Seleccione un caramelo</option>
                                {php}
                                    while($row = $query->fetch_assoc()){
                                {/php}
                                <option value="{php}echo $row['idCarameloTC'];{/php}">{php}echo utf8_encode($row['nombreCaramelo'].' - '.$row['tipo']);{/php}</option>
                                {php}
                                    }   
                                {/php}
                            </select>
                        </div>
                        
                    </div>
                </div>

                <div class="col-md-12 columna">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="row">
                                    <label>Selecciona fecha de inicio:</label>
                                </div>
                                <div class="row">
                                <input class="form-control" type="date" name="fechaInicio" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Agrega un %:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="number" min="1" value="1" max="100" class="form-control" placeholder="%" name="porcentaje">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-push-6  col-md-4">
                         <button class="btn btn-info" type="submit">Agregar dulce</button>
                     </div>
                </div>
            </form>

            <br><!-- Salto de linea -->
            <br><!-- Salto de linea -->

            <div class="row">
                <div class="col-md-8 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="270" class="text-center info">Dulce</th> <!-- Table Data Head -->
                                <th width="5" class="text-center info">% Porcentaje</th> <!-- Table Data Head -->
                                <th width="5" class="text-center info">Fecha inicio</th>
                                <th width="5" class="text-center info">Fecha Fin</th>
                            </tr>
                        </thead>
                        <tbody>
                            {php}
                                while($row = $descuentos->fetch_assoc()){
                            {/php}
                            <tr> <!-- Table Row -->
                                <td width="270">{php}echo utf8_encode($row['nombreCaramelo'].' - '.$row['tipo']);{/php}</td> <!-- Table Data -->
                                <td width="5" class="text-center">{php}echo utf8_encode($row['porcentaje']);{/php}</td> <!-- Table Data -->
                                <td width="5" class="text-center">{php}echo utf8_encode($row['fechaIni']);{/php}</td> <!-- Table Data -->
                                <td width="5" class="text-center">{php}echo utf8_encode($row['fechaFin']);{/php}</td> <!-- Table Data -->
                            </tr>
                            {php}
                                }   
                            {/php}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row contenedorPrincipal">
            <div class="col-md-2">
                <button class="btn btn-danger" type="submit">Eliminar</button>
            </div>
            <div class="col-md-1 col-md-push-7">
                <button class="btn btn-primary" type="submit">Guardar</button>
            </div>
        </div>
    </div>
    <!-- /#page-wrapper -->
</div>


</body>

{include file="../Overall/Footer.tpl"}