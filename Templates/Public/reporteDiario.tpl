{include file="../Overall/Header.tpl"}

<link rel="stylesheet" href="assets/css/pedidos.css?v={0|rand:100}" media="screen">

<body>
<div id="wrapper">
    {include file='../Overall/Navbar_admin.tpl'}
    <div id="page-wrapper">
        <div class="col-md-12 contenedorPrincipal">
            <div class="col-md-12 columna">
                <br>
                <br>
                <br>

                <div class="row">
                    <div class="col-md-7">
                        <input type="text" class="form-control" placeholder="Buscar por número">
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-info" type="submit">Buscar</button>
                    </div>
                </div>
                <br>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <label>Diario dulce:</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered margenIz ">
                        <thead>
                        <tr>
                            <th width="260" class="text-center info">Producto</th> <!-- Table Data Head -->
                            <th width="20" class="text-center info">Porcentaje</th> <!-- Table Data Head -->
                            <th width="20" class="text-center info">Fecha inicio descuento</th>
                            <th width="20" class="text-center info">Fecha fin descuento</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach key=key item=item from=$reporte}
                        <tr> <!-- Table Row -->
                            <td width="270">{$item['nombreCaramelo']} tipo {$item['tipo']}</td> <!-- Table Data -->
                            <td width="5" class="text-center">{$item['porcentaje']}</td> <!-- Table Data -->
                            <td width="5" class="text-center">{$item['fechaIni']}</td>
                            <td width="5" class="text-center">{$item['fechaFin']}</td>
                        </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-md-offset-10">
                    <br>
                    <button class="btn btn-danger" type="submit">Cancelar Pedido</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

{include file="../Overall/Footer.tpl"}