{include file="../Overall/Header.tpl"}

<link rel="stylesheet" href="assets/css/inventario.css?v={0|rand:100}" media="screen">

<body>
    <div id="wrapper">
        {include file='../Overall/Navbar_admin.tpl'}
        <div id="page-wrapper">
            <div class="col-md-12 contenedorPrincipal">
                <div class="col-md-12 columna">
                        <br>
                        <br>
                        <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Caramelos en pasillos:</label>
                            
                        </div>
                    </div>
                        <div class="col-md-12">

                            <table cellpadding="0" cellspacing="0" border="0" class="dataTable table table-striped" id="tabla">
                            {*<table class="table table-bordered margenIz ">*}
                                <thead>
                                <tr>
                                    <th width="150" class="text-center info">Producto</th> <!-- Table Data Head -->
                                    <th width="30" class="text-center info">Pasillo</th> <!-- Table Data Head -->
                                    <th width="30" class="text-center info">Cantidad</th>
                                    <th width="20" class="text-center info">Alerta</th>
                                    <th width="20" class="text-center info">Accion</th>
                                </tr>
                                </thead>
                                <tbody>
                                    
                                    {foreach from=$inventario item=item key=key}
                                
                                    <tr> <!-- Table Row -->
                                        <td width="150" class="text-center">{$item['nombre_Caramelo']} tipo {$item['descripcion_TC']}</td> <!-- Table Data -->
                                        <td width="5" class="text-center">{$item['numeroPasillo']}</td> <!-- Table Data -->
                                        <td width="5" class="text-center">{$item['cantidadProducto_P']}</td>
                                        {if ($item['cantidadProducto_P'] <=  20)}       
                                            <td width="20" style="background-color: red; color: red;">poco</td>
                                            
                                            <td>
                                                <form action="inventario_pasillo.php" method="post">
                                                    <input type="hidden" name="idInventario" value="{$item['idInventario']}">
                                                    <input type="hidden" name="actualCantidad" value="{$item['cantidadProducto_P']}">
                                                    <button type="submit" class="btn btn-info">Recarga</button>
                                                </form>
                                            </td>
                                                    
                                        {else}
                                            <td width="20" style="background-color: green; color: green;">mucho</td>
                                            <td></td>
                                        {/if}

                                    </tr>
                                    {/foreach}
                                

                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</body>


<script type="text/javascript">
    $(document).ready(function() {
        {literal}

        var myTable = $('#tabla').DataTable({
            "sPaginationType": "full_numbers",
            dom: 'Bfrtip',        // element order: NEEDS BUTTON CONTAINER (B) ****
            select: 'single',     // enable single row selection
            altEditor: true,      // Enable altEditor ****
            buttons: [
                // {text: 'Agregar',name: 'add'},
                // {extend: 'selected',text: 'Editar',name: 'edit'},
                // {extend: 'selected',text: 'Eliminar',name: 'delete'}
            ]
        });
    });

    $(document).ready(function() {
        var t = $('#tabla').DataTable();
        var counter = 1;

        $('#add').on( 'click', function () {
            t.row.add( [
                'Dulce #' + counter,
                'Cantidad',
                'Precio U.',
                'Precio T.'
            ] ).draw( false );

            counter++;
        } );

        $('#delete').on('click', function () {
            var data = t
                .rows()
                .data();


            console.log(data[0][0])
            alert( 'The table has ' + data.length + ' records' );
        });

    } );
    {/literal}
</script>


{include file="../Overall/Footer.tpl"}
