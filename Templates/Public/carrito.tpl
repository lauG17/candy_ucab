{include file="../Overall/Header.tpl"}
<div id="wrapper">
  {include file='../Overall/Navbar_admin.tpl'}
  <div id="page-wrapper">
    <div class="col-md-12 contenedorPrincipal">
      <div class="col-md-12">
        <h3>Caramelos a seleccionar :</h3>
      </div>

      <form action="presupuesto.php" method="post">
        <div class="row">
          <div class="col-md-12">
            <h4>
              {foreach from=$caramelos key=key item=item}
              <div class="form-check">
                  <label class="checkbox-inline">
                      <input type="checkbox" value="{$item['nombre']} tipo  {$item['tipo']}" class="checkCaramelos" name="caramelo[{$key}][Descripcion]" id="check_{$key}">{$item['nombre']} tipo  {$item['tipo']}</label>
                      <input type="hidden" id="id_{$key}" name="caramelo[{$key}][carameloId]" value="{$item['carameloId']}" disabled>
                      <input type="hidden" id="idTC_{$key}" name="caramelo[{$key}][carameloIdTC]" value="{$item['idTipo']}" disabled>
                    <span class="label label-default">{$item['costo']}</span>
                    <input type="hidden" id="costo_{$key}" name="caramelo[{$key}][Precio]" value="{$item['costo']}"disabled>
                    <input type="hidden" id="descuento_{$key}" name="caramelo[{$key}][Descuento]" value="{$item['descuento']}" disabled>
                    <input type="hidden" id="inventario_{$key}" name="caramelo[{$key}][idInventario]" value="{$item['idInventario']}" disabled>
              </div>
              <div class="form-group">
                <input type="number" min="1" max="{$item['existencia']}" class="form-control" id="cantidad_{$key}" name="caramelo[{$key}][Cantidad]"  placeholder="Cantidad a comprar" disabled>
              </div>
              {/foreach}
              <input type="hidden" id="clienteUsuario" name="clienteUsuario" value="{$clienteUsuario}">
            </h4>
            </div>
          </div>
        <div class="row">
          <div class="col-md-3">
            <button type="submit" class="btn btn-info">Pagar</button> 
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="assets/js/carrito.js"></script>
