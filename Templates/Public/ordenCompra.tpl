{include file="../Overall/Header.tpl"}

<link rel="stylesheet" href="assets/css/pedidos.css?v={0|rand:100}" media="screen">

<body>
<div id="wrapper">
    {include file='../Overall/Navbar_admin.tpl'}
    <div id="page-wrapper">
        <div class="col-md-12 contenedorPrincipal">


<div class="row">
    <div class="col-md-6">
        <label>Ordenes de compra:</label>
    </div>
</div>
<div class="col-md-12">
    <table class="table table-bordered margenIz ">
        <thead>
        <tr>
            <th width="240" class="text-center info">Producto</th> <!-- Table Data Head -->
            <th width="20" class="text-center info">Cantidad</th> <!-- Table Data Head -->
            <th width="20" class="text-center info">Fabrica</th>
            <th width="20" class="text-center info">Precio Unitario</th>
            <th width="20" class="text-center info">Status</th>
            <th width="20" class="text-center info">Accion</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$ordenes item=item key=key}
        <tr> <!-- Table Row -->
            <td width="265">{$item['nombre_Caramelo']} tipo {$item['descripcion_TC']}</td> <!-- Table Data -->
            <td width="5" class="text-center">{$item['cantidad']}</td> <!-- Table Data -->
            <td width="5" class="text-center"> {$item['nombreFabrica']}</td>
            <td width="5" class="text-center"> {$item['precioU']}</td>
            <td width="5" class="text-center">{$item['statusOrden']}</td>
            <td width="5" class="text-center"><button type="button" class="btn btn-info rounded-circle btnOrden"  idPedido="{$item['idPedidoFabrica']}"> Elegir Fabrica </td>
        </tr>
        {/foreach}
        </tbody>
    </table>
</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Fabricas donde se realizan ordenes :</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div>
          <form action="ordenC.php" method="post">
        <div class="row">
        <div class="col-xs-12">
          <select name="Fabricas" required>
           <option disabled selected value="">Seleccion</option>
              {foreach from=$Fabrica item=item_fab}
                  <option value="{$item_fab['cod_Fabrica']}">{$item_fab['nombre_Fabrica']} </option>
            {/foreach}
          </select>
          <input type="hidden" name="id_pedido_fabrica" id="id_pedido_fabrica" value="">
          <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Enviar</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
      </div>
      </div>
    </form>
      </div>



<script src="assets/js/ordenC.js"></script>
