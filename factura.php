<?php

include ("Main/Comunes.php");

session_start();

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];
    //var_dump($_GET);
    //var_dump($_SESSION);
    $Total = $_GET['Totalpagar'];
    $descuento = $_GET['descuento'];
    $Productos = $_GET['productos'];
    $metodoPago = $_GET['metodoPago'];
    $idMetodo = $_GET['idMetodo'];
    $Productos = stripslashes($Productos);
    $Productos = json_decode($Productos, true);
    $clienteUsuario = $_GET['clienteUsuario'];
    $tienda = $_SESSION['tienda'];
    $cantidadTotal = 0;
    //var_dump($Productos);
    foreach ($Productos as $key => $value) {
        $cantidadTotal+= $value['Cantidad'];
    }
    
    if ($clienteUsuario == $_SESSION['user']) {
        $tipoCompra = 1;
        $compra = "Compra En linea";
    } else {
        $tipoCompra = 0;
        $compra = "Compra En Tienda";
    }

    $db = new Conexion();
    $db->set_charset("UTF8");

    $idCliente = $db->query('SELECT cn.cod_CN from cliente_natural cn where cn.CI_CN = '.$_GET['cedula']);
    $idCliente = $db->recorrer($idCliente);
    $idCliente = $idCliente[0]['cod_CN'];
    $db->query("INSERT into pedido(cantidadTotal_Pedido, descripcion_Pedido, montoTotal_Pedido, fk_ClienteN, fk_Status, fk_Tienda, presupuesto, tipo_compra) values ($cantidadTotal, '$compra', $Total, $idCliente, 1, $tienda, 0, $tipoCompra)");
    $idPedido = $db->insert_id;
    
    $db->query("SET SQL_MODE = ''");
    $db->query("INSERT into status_pedido(fk_status, fk_pedido, fecha_entradastatus) values (1, $idPedido, now() )");
    
    if ($metodoPago == "Tarjeta de Credito") {
        $db->query("INSERT into pago(monto_Pago, fk_TDC) values ($Total, $idMetodo)");
        $idPago = $db->insert_id;
      } elseif ($metodoPago =="Tarjeta de Debito") {
        $db->query("INSERT into pago(monto_Pago, fk_TDD) values ($Total, $idMetodo)");
        $idPago = $db->insert_id;
      } elseif ($metodoPago == "Cheque") {
        $numero = $_GET['numero'];
        $db->query("INSERT into cheque(numero_cheque, fk_ClienteN) values ($numero, $idCliente)");
        $idCheque = $db->insert_id;
        $db->query("INSERT into pago(monto_Pago, fk_Cheque) values ($Total, $idCheque)");
        $idPago = $db->insert_id;
       
      } elseif ($metodoPago == "Efectivo") {
        $descripcion = $_GET['descripcion'];
        $idCliente = $db->query("SELECT cn.cod_CN idCn FROM usuario u, cliente_natural cn where u.fk_clienteN = cn.cod_CN and u.user_Usuario = '".$clienteUsuario."'");
        $idCliente = $db->recorrer($idCliente);
        $idCliente = $idCliente[0]['idCn'];
        $db->query("INSERT into efectivo(descripcion_Efectivo, fk_ClienteN) values ('rtter', $idCliente)");
        $idEfectivo = $db->insert_id;
        $db->query("INSERT into pago(monto_Pago, fk_Efectivo) values ($Total, $idEfectivo)");
        $idPago = $db->insert_id;
      }
    
    foreach ($Productos as $key => $value) {
        $idTipo = $value['idTipo'];
        $inventarioId = $value['idInventario'];
        $cantidad = $value['Cantidad'];
        /*Inserto los productos comprados en pedido_pago */
        $db->query("INSERT into pedido_pago(fk_Pedido, fk_Pago, fk_Caramelo) values ($idPedido, $idPago, $idTipo)");
        /* actualizo la cantidad restante en pasillo */
        $db->query("UPDATE inventario set cantidadProducto_P = (cantidadProducto_P - $cantidad) where cod_I = $inventarioId");
    }

    $db->query("INSERT into punto(cantidad_punto, valor_punto, fk_clienteN, fk_Pago, fk_Pedido) values ($cantidadTotal, 100, $idCliente, $idPago, $idPedido)");




    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Presupuesto',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        "total" => $Total,
        "Descuento" => $descuento,
        "Productos" => $Productos,
        "metodo" => $metodoPago

    ));
    $template->display("Public/factura.tpl");
}
?>
