<?php

include ("Main/Comunes.php");

session_start();

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];
    $db = new Conexion();
    $db->set_charset("UTF8");
    $template = new CandyUCAB();

    if ($_SERVER["REQUEST_METHOD"] == "GET") {

        $fabrica = $db->query("Select nombre_Fabrica, cod_Fabrica from fabrica");
        $fabrica= $db->recorrer($fabrica);
        $ordenesCompra = $db->query("select pf.cod_PF idPedidoFabrica,  f.nombre_Fabrica nombreFabrica, pf.cantidad_PF cantidad, s.nombre_Status statusOrden, c.nombre_Caramelo, tc.descripcion_TC, pf.precioU precioU from pedido_fabrica pf  left join fabrica f on pf.fk_fabrica = f.cod_Fabrica , status s, inventario i, caramelo_tc ctc, caramelo c, tipo_caramelo tc, pasillo p where pf.fk_status = s.cod_Status and pf.fk_inventario = i.cod_I and i.fk_pasillo = p.cod_Pasillo and i.fk_Caramelo = ctc.cod_CTC and ctc.fk_Caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC and p.fk_Tienda = ".$_SESSION['tienda']);
        $ordenesCompra= $db->recorrer($ordenesCompra);
      
  
  
     
      $template->assign(array(
          'page_name' => 'Orden de Compra',
          'login' => true,
          'name' => $_SESSION['name'],
          'user' => $_SESSION['user'],
          'rol' => $_SESSION['rol'],
          'tienda' => $_SESSION['tienda'],
          'Fabrica'=> $fabrica,
          'ordenes' => $ordenesCompra
      ));
      $template->display("Public/ordenCompra.tpl");
    } else {
        //var_dump($_POST);
        $fabricaId = $_POST['Fabricas'];
        $idPedidoFabrica = $_POST['id_pedido_fabrica'];
        /** seteo fabrica en orden de compra */
        $db->query("UPDATE pedido_fabrica, inventario set fk_fabrica = $fabricaId, fk_status = 4, cantidadProducto_I = (cantidadProducto_I + 10000) where cod_PF = $idPedidoFabrica and fk_inventario = cod_I");
        /** inserto en historial de status */
        $db->query("SET SQL_MODE = ''");
        $db->query("UPDATE status_pedido set fecha_salidastatus = now() where fecha_salidastatus = '0000:00:00 00:00:00' and fk_pedido_fabrica = $idPedidoFabrica");
        $db->query("INSERT into status_pedido(fk_status, fk_pedido_fabrica, fecha_entradastatus) values (4, $idPedidoFabrica, now() )");
        //echo $db->error;
        $fabrica = $db->query("Select nombre_Fabrica, cod_Fabrica from fabrica");
        $fabrica= $db->recorrer($fabrica);
        $ordenesCompra = $db->query("select pf.cod_PF idPedidoFabrica,  f.nombre_Fabrica nombreFabrica, pf.cantidad_PF cantidad, s.nombre_Status statusOrden, c.nombre_Caramelo, tc.descripcion_TC, pf.precioU precioU from pedido_fabrica pf  left join fabrica f on pf.fk_fabrica = f.cod_Fabrica , status s, inventario i, caramelo_tc ctc, caramelo c, tipo_caramelo tc, pasillo p where pf.fk_status = s.cod_Status and pf.fk_inventario = i.cod_I and i.fk_pasillo = p.cod_Pasillo and i.fk_Caramelo = ctc.cod_CTC and ctc.fk_Caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC and p.fk_Tienda = ".$_SESSION['tienda']);
        $ordenesCompra= $db->recorrer($ordenesCompra);
      

      $template->assign(array(
          'page_name' => 'Orden de Compra',
          'login' => true,
          'name' => $_SESSION['name'],
          'user' => $_SESSION['user'],
          'rol' => $_SESSION['rol'],
          'tienda' => $_SESSION['tienda'],
          'Fabrica'=> $fabrica,
          'ordenes' => $ordenesCompra
      ));
      $template->display("Public/ordenCompra.tpl");
    }
      
}




?>
