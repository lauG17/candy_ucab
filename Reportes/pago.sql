/*Marca mas registrada de tarjetas de credito*/
SELECT marca_TDC as 'Marca mas registrada', count(marca_TDC)  'Cantidad de veces'
  from tarjeta_credito
  group by marca_TDC
  order by 'Cantidad de veces' desc
  limit 1;
