/*Materia prima mas usada */

SELECT tipo_MP as 'Materia mas utilizada', sum(cantidad_CMP) AS 'Cantidad utilizada'
from materia_prima, caramelo_mp
where fk_materiaPrima = cod_MP
group by  tipo_MP
order by sum(cantidad_CMP) desc
limit 1;

