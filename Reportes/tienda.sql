/*egresos e ingresos por tienda*/

SELECT T.nombre_Tienda,T.cod_Tienda, sum(P.montoTotal_Pedido) as Ingresos, sum(PF.cantidad_PF * PF.precioU) as Egresos
from pedido P, pedido_fabrica PF, tienda T, inventario I, pasillo Pa
where T.cod_Tienda = P.fk_Tienda and PF.fk_inventario = I.cod_I and I.fk_Pasillo = Pa.cod_Pasillo and Pa.fk_Tienda = T.cod_Tienda
group by T.nombre_Tienda, T.cod_Tienda;

/*Productos mas vendidos por tienda*/

select nombre_tienda as Tienda, nombre_Caramelo as Caramelo, count(PG.fk_caramelo) as Ventas
from caramelo, pedido_pago PG, tienda, pedido P
where cod_Caramelo = PG.fk_Caramelo and PG.fk_Pedido =  P.cod_Pedido and P.fk_Tienda = cod_Tienda
group by Tienda, Caramelo  
order by Ventas desc;