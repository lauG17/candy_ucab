/*top 10 clientes frecuentes con mayor numero de compras */

(select nombre_Tienda as Tienda,nombre_CN as Cliente, apellido1_CN as 'Primer apellido', CI_CN as Cedula, count(P.fk_ClienteN) as Cantidad 
from cliente_natural, tienda, pedido P
where cod_Tienda = P.fk_tienda and  cod_CN = P.fk_ClienteN
group by nombre_Tienda, nombre_CN, apellido1_CN, CI_CN)
union
(select nombre_Tienda as Tienda,DenominacionComercial_CJ as Cliente, RazonSocial_CJ as 'Primer apellido', RIF_CJ as Cedula, count(P.fk_ClienteJ) as Cantidad
from cliente_juridico, tienda, pedido P
where cod_Tienda = P.fk_tienda and  cod_CJ = P.fk_ClienteJ
group by nombre_Tienda, DenominacionComercial_CJ, RazonSocial_CJ, RIF_CJ)
order by Cantidad desc
limit 10;


/*top 5 mejores clientes segun compras*/

(select nombre_CN as Cliente, apellido1_CN as 'Primer apellido', CI_CN as Identificacion, count(P.fk_ClienteN) as 'Cantidad de compras'
from cliente_natural, pedido P 
where cod_CN = P.fk_ClienteN
group by nombre_CN, apellido1_CN, CI_CN  
order by count(P.fk_ClienteN) desc)
union 
(select DenominacionComercial_CJ as Cliente, RazonSocial_CJ as 'Primer apellido', RIF_CJ as Identificacion, count(P.fk_ClienteJ) as 'Cantidad de compras'
from cliente_juridico, pedido P
where cod_CJ= P.fk_ClienteJ
group by DenominacionComercial_CJ, RazonSocial_CJ, RIF_CJ   
order by count(P.fk_ClienteJ) desc)
limit 5;