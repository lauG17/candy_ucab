<?php

include ("Main/Comunes.php");

session_start();


if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];

    $db = new Conexion();
    $db->set_charset("UTF8");

    $reporte = $db->query("SELECT T.nombre_Tienda,T.cod_Tienda, sum(P.montoTotal_Pedido) as Ingresos, sum(PF.cantidad_PF * PF.precioU) as Egresos
    from pedido P, pedido_fabrica PF, tienda T, inventario I, pasillo Pa
    where T.cod_Tienda = P.fk_Tienda and PF.fk_inventario = I.cod_I and I.fk_Pasillo = Pa.cod_Pasillo and Pa.fk_Tienda = T.cod_Tienda
    group by T.nombre_Tienda, T.cod_Tienda;");
    $reporte = $db->recorrer($reporte);
    //var_dump($reporte);

    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Reporte',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        'tienda' => $_SESSION['tienda'],
        'reporte' => $reporte
    ));
    $template->display("Public/reporteIngresosEgresosTienda.tpl");
}
?>