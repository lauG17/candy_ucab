<?php

include ("Main/Comunes.php");

session_start();

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];
    $tienda = $_SESSION['tienda'];

    $db = new Conexion();
    $db->set_charset("UTF8");
    //var_dump($tienda);
    $caramelos = $db->query("SELECT c.cod_Caramelo carameloId, c.nombre_caramelo nombre, tc.descripcion_TC tipo, ctc.cod_CTC idTipo, c.costo_Caramelo costo, i.cantidadProducto_P existencia, 0 descuento, i.cod_I idInventario FROM caramelo c, inventario i, pasillo p, caramelo_tc ctc, tipo_caramelo tc where i.fk_Caramelo = ctc.cod_CTC and ctc.fk_Caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC  and i.fk_Pasillo = p.cod_Pasillo and i.cantidadProducto_P > 0 and p.fk_Tienda = ".$tienda);
    $caramelos = $db->recorrer($caramelos);
    //var_dump($caramelos);
    $descuentos = $db->query("SELECT ctc.cod_CTC idCarameloTC, c.cod_Caramelo carameloId, c.nombre_Caramelo nombreCaramelo, tc.descripcion_TC tipo, d.descripcion_descuento porcentaje, date(d.finicio_descuento) fechaIni, date(d.fFinal_descuento) fechaFin FROM caramelo c, caramelo_tc ctc, tipo_caramelo tc, descuento d, cs_descuento csd where ctc.fk_caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC and csd.fk_descuento = d.cod_descuento and csd.fk_Caramelo = ctc.cod_CTC and date(now()) between date(d.finicio_descuento) and date(d.fFinal_descuento)");
    $descuentos = $db->recorrer($descuentos);
    //var_dump($caramelos);
    //var_dump($descuentos);
    foreach ($caramelos as $key => $value) {
        foreach ($descuentos as $keyDescuento => $valueDescuento) {
            if($value['carameloId'] == $valueDescuento['carameloId']){
                $caramelos[$key]['descuento'] = $valueDescuento['porcentaje'];
                break;
            }
        }
    }

    $clienteUsuario = null;
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $cedulaUsuario = $_POST['cedula'];
        $clienteUsuario = $db->query("select u.user_Usuario from usuario u, cliente_natural cn where u.fk_clienteN = cn.cod_CN and cn.CI_CN = $cedulaUsuario");
        $clienteUsuario = $db->recorrer($clienteUsuario);
        $clienteUsuario = $clienteUsuario[0]['user_Usuario'];
        if (!$clienteUsuario) {
            header('location: compraTienda.php');
        }
    } else {
        $clienteUsuario = $_SESSION['user'];
    }
    var_dump($clienteUsuario);
    //var_dump($caramelos);
    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Carrito',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        'caramelos' => $caramelos,
        'clienteUsuario' => $clienteUsuario
    ));
    $template->display("Public/carrito.tpl");
}
?>
