<?php

include ("Main/Comunes.php");

session_start();

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];

    $db = new Conexion();
    $db->set_charset("UTF8");

    $reporte = $db->query("select p.Nombre_personal nombre, p.apellido1_personal primerApellido, p.apellido2_personal segundoApellido,
d.nombre_Departamento departamento, t.nombre_Tienda tienda, cl.FechaInicial_CL entrada, cl.FechaSalida_CL salida
from personal p, departamento d, tienda t, control_laboral cl
where cl.fk_personal = p.CI_personal and p.fk_departamento = d.cod_Departamento
and d.fk_Tienda = t.cod_Tienda");
    $reporte = $db->recorrer($reporte);
    //var_dump($reporte);

    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Reporte',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        'tienda' => $_SESSION['tienda'],
        'reporte' => $reporte
    ));
    $template->display("Public/reporteAsistencia.tpl");
}
?>