<?php

include ("Main/Comunes.php");

session_start();

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];

    $db = new Conexion();
    $db->set_charset("UTF8");

    $reporte = $db->query("SELECT ctc.cod_CTC idCarameloTC, c.nombre_Caramelo nombreCaramelo, tc.descripcion_TC tipo, 
    d.descripcion_descuento porcentaje, 
    date(d.finicio_descuento) fechaIni, date(d.fFinal_descuento) fechaFin 
    FROM caramelo c, caramelo_tc ctc, tipo_caramelo tc, descuento d, cs_descuento csd 
    where ctc.fk_caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC 
    and csd.fk_descuento = d.cod_descuento and csd.fk_Caramelo = ctc.cod_CTC
    and ((date(d.finicio_descuento) >= date(now()) and  date(d.finicio_descuento) <= date_add( date(now()), INTERVAL 25 DAY)) 
        or (date(d.fFinal_descuento) >= date(now()) and  date(d.fFinal_descuento) <= date_add( date(now()), INTERVAL 25 DAY)));");
    $reporte = $db->recorrer($reporte);
    //var_dump($reporte);

    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Diario',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        'tienda' => $_SESSION['tienda'],
        'reporte' => $reporte
    ));
    $template->display("Public/reporteDiario.tpl");
}
?>