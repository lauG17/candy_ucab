<?php

include ("Main/Comunes.php");

session_start();


if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];

    $db = new Conexion();
    $db->set_charset("UTF8");

    $reporte = $db->query("(select nombre_CN as Cliente, apellido1_CN as 'Primer apellido', CI_CN as Identificacion, count(P.fk_ClienteN) as 'Cantidad de compras'
    from cliente_natural, pedido P 
    where cod_CN = P.fk_ClienteN
    group by nombre_CN, apellido1_CN, CI_CN  
    order by count(P.fk_ClienteN) desc)
    union 
    (select DenominacionComercial_CJ as Cliente, RazonSocial_CJ as 'Primer apellido', RIF_CJ as Identificacion, count(P.fk_ClienteJ) as 'Cantidad de compras'
    from cliente_juridico, pedido P
    where cod_CJ= P.fk_ClienteJ
    group by DenominacionComercial_CJ, RazonSocial_CJ, RIF_CJ   
    order by count(P.fk_ClienteJ) desc)
    limit 5;");
    $reporte = $db->recorrer($reporte);
    //var_dump($reporte);

    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Reporte',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        'tienda' => $_SESSION['tienda'],
        'reporte' => $reporte
    ));
    $template->display("Public/reporteMejoresClientes.tpl");
}
?>