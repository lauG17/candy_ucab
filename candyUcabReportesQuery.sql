/*DIARIO DULCE*/
SELECT ctc.cod_CTC idCarameloTC, c.nombre_Caramelo nombreCaramelo, tc.descripcion_TC tipo, 
d.descripcion_descuento porcentaje, 
date(d.finicio_descuento) fechaIni, date(d.fFinal_descuento) fechaFin 
FROM caramelo c, caramelo_tc ctc, tipo_caramelo tc, descuento d, cs_descuento csd 
where ctc.fk_caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC 
and csd.fk_descuento = d.cod_descuento and csd.fk_Caramelo = ctc.cod_CTC
and ((date(d.finicio_descuento) >= date(now()) and  date(d.finicio_descuento) <= date_add( date(now()), INTERVAL 25 DAY)) 
	or (date(d.fFinal_descuento) >= date(now()) and  date(d.fFinal_descuento) <= date_add( date(now()), INTERVAL 25 DAY)));


/*Reporte de asistencia indicando hora de entrada , hora salida, cédula del empleado,
nombres y apellidos y departamento.*/
select p.Nombre_personal nombre, p.apellido1_personal primerApellido, p.apellido2_personal segundoApellido,
d.nombre_Departamento departamento, t.nombre_Tienda tienda, cl.FechaInicial_CL entrada, cl.FechaSalida_CL salida
from personal p, departamento d, tienda t, control_laboral cl
where cl.fk_personal = p.CI_personal and p.fk_departamento = d.cod_Departamento
and d.fk_Tienda = t.cod_Tienda and t.cod_Tienda = 1