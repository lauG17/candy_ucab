<?php

include ("Main/Comunes.php");

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];
    $template = new CandyUCAB();
    $db = new Conexion();
    $db->set_charset("UTF8");
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        $inventarioPasillo = $db->query("Select i.cantidadProducto_P, i.cod_I idInventario, p.numeroPasillo , c.nombre_Caramelo, tc.descripcion_TC from inventario i, pasillo p, caramelo_tc ctc, caramelo c, tipo_caramelo tc where i.fk_Caramelo = ctc.cod_CTC and i.fk_Pasillo = p.cod_Pasillo and ctc.fk_Caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC and p.fk_Tienda = ".$_SESSION['tienda']);
        $inventarioPasillo = $db->recorrer($inventarioPasillo);
        $template->assign(array(
            'page_name' => 'Inventario',
            'login' => true,
            'name' => $_SESSION['name'],
            'user' => $_SESSION['user'],
            'rol' => $_SESSION['rol'],
            'tienda' => $_SESSION['tienda'],
            'inventario' => $inventarioPasillo
        ));
        //var_dump($inventarioPasillo);
        $template->display("Public/inventario_pasillo.tpl");
    } else {
        //var_dump($_POST);
        $recarga = $_POST['actualCantidad'];
        $recarga = 50 - $recarga;
        $idInventario = $_POST['idInventario'];
        /* Actualizo inventario pasillo y almacen*/
        $db->query("update inventario set cantidadProducto_P = ($recarga + cantidadProducto_P), cantidadProducto_I = (cantidadProducto_I - $recarga) where cod_I = $idInventario");

        $inventarioPasillo = $db->query("Select i.cantidadProducto_P, i.cantidadProducto_I,  i.cod_I idInventario, p.numeroPasillo , c.nombre_Caramelo, tc.descripcion_TC from inventario i, pasillo p, caramelo_tc ctc, caramelo c, tipo_caramelo tc where i.fk_Caramelo = ctc.cod_CTC and i.fk_Pasillo = p.cod_Pasillo and ctc.fk_Caramelo = c.cod_Caramelo and ctc.fk_tipoCaramelo = tc.cod_TC and p.fk_Tienda = ".$_SESSION['tienda']);
        $inventarioPasillo = $db->recorrer($inventarioPasillo);
        
        foreach ($inventarioPasillo as $key=>$value) {
            echo $value['idInventario'].' / '.$idInventario.' / '.$value['cantidadProducto_I'];
            if ($value['cantidadProducto_I'] < 100 && $value['idInventario'] == $idInventario) {
                /* genero oreden de compra*/
                $db->query("INSERT into pedido_fabrica (cantidad_PF, fk_status, fk_inventario, precioU) values (10000, 5, $idInventario, 80)");
                $idPedidoFabrica = $db->insert_id;
                /* Guardo status en el historial */
                $db->query("SET SQL_MODE = ''");
                $db->query("INSERT into status_pedido(fk_status, fk_pedido_fabrica, fecha_entradastatus) values (5, $idPedidoFabrica, now() )");
                echo $db->error;
                break;
            }
        }
        
        $template->assign(array(
            'page_name' => 'Inventario',
            'login' => true,
            'name' => $_SESSION['name'],
            'user' => $_SESSION['user'],
            'rol' => $_SESSION['rol'],
            'tienda' => $_SESSION['tienda'],
            'inventario' => $inventarioPasillo
        ));
        //var_dump($inventarioPasillo);
        $template->display("Public/inventario_pasillo.tpl");

    }
}
?>
