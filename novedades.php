<?php

    include ("Main/Comunes.php");
    

    session_start();

    $db = new Conexion();
    $db->set_charset("UTF8");
    $template = new CandyUCAB();
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        if (isset($_SESSION['user'])) {
            #echo 'Has iniciado sesion ',$_SESSION['name'];
            
            $template->assign(array(
                'page_name' => 'Novedades',
                'login' => true,
                'name' => $_SESSION['name'],
                'user' => $_SESSION['user'],
                'rol' => $_SESSION['rol'],
                'tienda' => $_SESSION['tienda']
            ));
            $template->display("Public/novedades.tpl");
        }
    } else {
        $idCarameloTC = $_POST['caramelo-tipo'];
        
        $fechaIni = date_create($_POST['fechaInicio']);
        $fechaFin = date_create($_POST['fechaInicio']);
        date_add($fechaFin, date_interval_create_from_date_string('10 days'));
        $fechaFin = date_format($fechaFin, 'Y-m-d H:i:s');
        $fechaIni = date_format($fechaIni, 'Y-m-d H:i:s');
        $porcentaje = $_POST['porcentaje'];

        
       
        $db->query('INSERT INTO descuento(descripcion_descuento, finicio_descuento, fFinal_descuento) values ('.$porcentaje.',"'.$fechaIni.'", "'.$fechaFin.'")');
        $idDescuento = $db->insert_id;
        $db->query('INSERT INTO cs_descuento(fk_descuento, fk_Caramelo) values ('.$idDescuento.', '.$idCarameloTC.')');
        echo $db->error;
        $template->assign(array(
            'page_name' => 'Novedades',
            'login' => true,
            'name' => $_SESSION['name'],
            'user' => $_SESSION['user'],
            'rol' => $_SESSION['rol']
        ));
            
        $template->display("Public/novedades.tpl");
    }
    
?>