<?php

include ("Main/Comunes.php");

session_start();

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];

    $db = new Conexion();
    $db->set_charset("UTF8");

    $reporte = $db->query("(select nombre_Tienda as Tienda,nombre_CN as Cliente, apellido1_CN as 'Primer apellido', CI_CN as Cedula, count(P.fk_ClienteN) as Cantidad 
    from cliente_natural, tienda, pedido P
    where cod_Tienda = P.fk_tienda and  cod_CN = P.fk_ClienteN
    group by nombre_Tienda, nombre_CN, apellido1_CN, CI_CN)
    union
    (select nombre_Tienda as Tienda,DenominacionComercial_CJ as Cliente, RazonSocial_CJ as 'Primer apellido', RIF_CJ as Cedula, count(P.fk_ClienteJ) as Cantidad
    from cliente_juridico, tienda, pedido P
    where cod_Tienda = P.fk_tienda and  cod_CJ = P.fk_ClienteJ
    group by nombre_Tienda, DenominacionComercial_CJ, RazonSocial_CJ, RIF_CJ)
    order by Cantidad desc
    limit 10;");
    $reporte = $db->recorrer($reporte);
    //var_dump($reporte);

    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Reporte',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        'tienda' => $_SESSION['tienda'],
        'reporte' => $reporte
    ));
    $template->display("Public/reporteClientesFrecuentes.tpl");
}
?>