<?php

include ("Main/Comunes.php");

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];

    $Total = $_GET['Totalpagar'];
    $metodo = $_GET['Metodopago'];
    $descuento = $_GET['descuento'];
    $title = "Pagar";
    $productos = $_GET['productosArray'];
    $usuario = $_GET['clienteUsuario'];
    var_dump($_GET);
    
    $db = new Conexion();
    $db->set_charset("UTF8");
    $datosPago = null;
    if ($metodo == "TarjetaC") {
      $title = "Tarjeta de Credito";
      $datosPago = $db->query("SELECT cn.CI_CN ci, tdc.cod_TDC idM, tdc.numero_TDC numeroT, tdc.Fecha_Vencimiento_TDC vencimiento FROM usuario u, cliente_natural cn, tarjeta_credito tdc where u.fk_clienteN = cn.cod_CN and tdc.fk_clienteN = cn.cod_CN and u.user_Usuario = '".$usuario."'");
      $datosPago = $db->recorrer($datosPago);
    } elseif ($metodo == "TarjetaD") {
      $title = "Tarjeta de Debito";
      $datosPago = $db->query("SELECT cn.CI_CN ci, tdd.cod_TDD idM, tdd.numero_TDD numeroT FROM usuario u, cliente_natural cn, tarjeta_debito tdd where u.fk_clienteN = cn.cod_CN and tdd.fk_clienteN = cn.cod_CN and u.user_Usuario = '".$usuario."'");
      $datosPago = $db->recorrer($datosPago); 
    } elseif ($metodo == "Cheque") {
      $title = "Cheque";
      $datosPago = $db->query("SELECT cn.CI_CN ci FROM usuario u, cliente_natural cn where u.fk_clienteN = cn.cod_CN and u.user_Usuario = '".$usuario."'");
      $datosPago = $db->recorrer($datosPago); 
      $datosPago[0]["idM"] = null;
    } elseif ($metodo == "Efectivo") {
      $title = "Efectivo";
    }

    //var_dump($datosPago);

    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Presupuesto',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        "title" => $title,
        "total" => $Total,
        "descuento" => $descuento,
        "productos" => $productos,
        "datosPago" => $datosPago[0],
        "clienteUsuario" => $usuario
    ));
    $template->display("Public/pagos.tpl");
}
?>
