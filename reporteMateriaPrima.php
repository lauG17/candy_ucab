<?php

include ("Main/Comunes.php");

session_start();

if (isset($_SESSION['user'])) {
    #echo 'Has iniciado sesion ',$_SESSION['name'];

    $db = new Conexion();
    $db->set_charset("UTF8");

    $reporte = $db->query("SELECT tipo_MP as 'Materia mas utilizada', sum(cantidad_CMP) AS 'Cantidad utilizada'
    from materia_prima, caramelo_mp
    where fk_materiaPrima = cod_MP
    group by  tipo_MP
    order by sum(cantidad_CMP) desc
    limit 1;");
    $reporte = $db->recorrer($reporte);
    //var_dump($reporte);

    $template = new CandyUCAB();
    $template->assign(array(
        'page_name' => 'Reporte',
        'login' => true,
        'name' => $_SESSION['name'],
        'user' => $_SESSION['user'],
        'rol' => $_SESSION['rol'],
        'tienda' => $_SESSION['tienda'],
        'reporte' => $reporte
    ));
    $template->display("Public/reporteMateriaPrima.tpl");
}
?>